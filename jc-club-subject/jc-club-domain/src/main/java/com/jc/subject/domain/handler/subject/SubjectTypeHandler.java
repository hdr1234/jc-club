package com.jc.subject.domain.handler.subject;

import com.jc.subject.common.enums.SubjectInfoTypeEnum;
import com.jc.subject.domain.entity.SubjectInfoBO;
import com.jc.subject.domain.entity.SubjectOptionBO;

public interface SubjectTypeHandler {

    /**
     * 枚举身份的识别
     */
    SubjectInfoTypeEnum getHandlerType();

    /**
     * 实际题目的插入
     */
    void add(SubjectInfoBO subjectInfoBO);

    /**
     * 实际题目的查询
     */
    SubjectOptionBO query(Long subjectId);

}

package com.jc.subject.domain.convert;

import com.jc.subject.domain.entity.SubjectAnswerBO;
import com.jc.subject.domain.entity.SubjectInfoBO;
import com.jc.subject.infra.basic.entity.SubjectInfo;
import com.jc.subject.infra.basic.entity.SubjectRadio;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubjectRadioConverter {

    SubjectRadioConverter INSTANCE = Mappers.getMapper(SubjectRadioConverter.class);

    SubjectRadio convertBOToEntity(SubjectAnswerBO subjectAnswerBO);

    List<SubjectAnswerBO> convertRadioToAnswerBOList(List<SubjectRadio> subjectRadioList);

}

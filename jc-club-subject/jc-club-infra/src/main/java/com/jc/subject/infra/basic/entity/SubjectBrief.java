package com.jc.subject.infra.basic.entity;

import java.util.Date;
import java.io.Serializable;
import lombok.Data;

/**
 * 简答题(SubjectBrief)实体类
 */
@Data
public class SubjectBrief implements Serializable {
    private static final long serialVersionUID = 610344594808584712L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 题目id
     */
    private Long subjectId;
    /**
     * 题目答案
     */
    private String subjectAnswer;
    /**
     * 创建人
     */
    private String createdBy;
    /**
     * 创建时间
     */
    private Date createdTime;
    /**
     * 更新人
     */
    private String updateBy;
    /**
     * 更新时间
     */
    private Date updateTime;
    /**
     * 是否删除 0: 未删除 1: 已删除
     */
    private Integer isDeleted;


}


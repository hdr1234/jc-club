package com.jc.subject.application.convert;

import com.jc.subject.application.dto.SubjectAnswerDTO;
import com.jc.subject.application.dto.SubjectCategoryDTO;
import com.jc.subject.application.dto.SubjectInfoDTO;
import com.jc.subject.domain.entity.SubjectAnswerBO;
import com.jc.subject.domain.entity.SubjectCategoryBO;
import com.jc.subject.domain.entity.SubjectInfoBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubjectInfoDTOConverter {

    SubjectInfoDTOConverter INSTANCE = Mappers.getMapper(SubjectInfoDTOConverter.class);

    List<SubjectInfoDTO> convertBoToInfoDTOList(List<SubjectInfoBO> subjectInfoBOList);

    SubjectInfoBO convertDTOToInfoBO(SubjectInfoDTO subjectInfoDTO);

    SubjectInfoDTO convertBOToInfoDTO(SubjectInfoBO subjectInfoBO);



}

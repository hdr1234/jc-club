package com.jc.subject.application.convert;

import com.jc.subject.application.dto.SubjectAnswerDTO;
import com.jc.subject.domain.entity.SubjectAnswerBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubjectAnswerDTOConverter {

    SubjectAnswerDTOConverter INSTANCE = Mappers.getMapper(SubjectAnswerDTOConverter.class);

    SubjectAnswerBO convertDTOToBO(SubjectAnswerDTO subjectAnswerDTO);

    List<SubjectAnswerBO> convertDTOToAnswerBOList(List<SubjectAnswerDTO> subjectAnswerDTOList);

}

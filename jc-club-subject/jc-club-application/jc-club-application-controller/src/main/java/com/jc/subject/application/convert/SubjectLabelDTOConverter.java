package com.jc.subject.application.convert;

import com.jc.subject.application.dto.SubjectLabelDTO;
import com.jc.subject.domain.entity.SubjectLabelBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import java.util.List;

/**
 * 标签dto转换器
 */
@Mapper
public interface SubjectLabelDTOConverter {

    SubjectLabelDTOConverter INSTANCE = Mappers.getMapper(SubjectLabelDTOConverter.class);

    List<SubjectLabelDTO> convertBoToLabelDTOList(List<SubjectLabelBO> subjectLabelBOList);

    SubjectLabelBO convertDTOToLabelBO(SubjectLabelDTO subjectLabelDTO);

}
